/*

Name:					Michael Kearney
Clemson Username:		kearne4
Lab Section Number:		001
Lab Number:				6
TA Names:				Nushrat Humaira
						John Choi

*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}

int main(int argc, char const *argv[]) {

  // IMPLEMENT as instructed below
  /*This is to seed the random generator */

	srand(unsigned (time(0)));

  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	
	cout << endl;

	employee roster[10];

	for(int i = 0; i < 10; i++){
		cout << "Last Name: ";
		cin >> roster[i].lastName;
		cout << "First Name: ";
		cin >> roster[i].firstName;
		cout << "Birth Year: ";
		cin >> roster[i].birthYear;
		cout << "Hourly Wage: ";
		cin >> roster[i].hourlyWage;
		cout << endl;
	}
	
  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

	random_shuffle(&roster[0], &roster[10], myrandom);

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/

	employee roster2[5];

	for(int i = 0; i < 5; i++){
		roster2[i] = roster[i];
	}

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/

	sort(&roster2[0], &roster2[5], name_order);

    /*Now print the array below */

	for(auto x: roster2){
		cout << right << setw(16) << x.lastName + ", " + x.firstName << endl;
		cout << right << setw(16) << x.birthYear << endl;
		cout << right << setw(16) << fixed << showpoint << setprecision(2) << x.hourlyWage << endl;
		cout << endl;
	}

	return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

bool name_order(const employee& lhs, const employee& rhs) {
  // IMPLEMENT
	return lhs.lastName < rhs.lastName;
}



